const assert = require('assert');
const http = require('http');

const app = require('./app'); // Assuming your Express app is in a file named app.js

const port = 3001;

// Start the server for testing
const server = http.createServer(app);
server.listen(port, function() {
  console.log('Testing server is listening on port', port);

  // Make a request to the server
  http.get('http://localhost:3000', function(response) {
    let data = '';

    response.on('data', function(chunk) {
      data += chunk;
    });

    response.on('end', function() {
      // Perform assertions on the response
      assert.strictEqual(response.statusCode, 200);
      assert.strictEqual(data, 'Hello, World!');
      console.log('Test passed!');
      server.close();
    });
  });
});
